package objectrepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PlansPage {

	WebDriver driver;

	public PlansPage(WebDriver driver) {
		this.driver = driver;
	}

	By signup = By.xpath(".//*[@id='pricingModel']/div/div[3]/div[4]/div[1]/div[2]/div[3]/a");

	public WebElement signup() {
		return driver.findElement(signup);
	}
}
