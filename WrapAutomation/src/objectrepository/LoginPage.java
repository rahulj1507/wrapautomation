package objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;

public class LoginPage {

	WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}

	By loginSignup = By.xpath(".//*[@id='wrap-theme']/div[2]/div[1]/wm-auth-page/div/div/wm-auth/div/div[3]/wm-login/div/div/a");

	public WebElement loginSignup() {
		return driver.findElement(loginSignup);
	}
}
