package objectrepository;

import java.util.Random;
import java.security.SecureRandom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Register2Page {
	private static final int PASSWORD_LENGTH = 8;
	private static final Random RANDOM = new SecureRandom();

	WebDriver driver;

	public Register2Page(WebDriver driver) {
		this.driver = driver;
	}
	
	By username = By.xpath(".//*[@id='wrap-theme']/div[2]/div[1]/wm-plans-page/div/div/div/wm-plans-authorization/div/div/wm-auth-page/div/ng-transclude/wm-auth/div/div[3]/wm-signup/div/div/form/div/b");
	By password = By.xpath(".//*[@id='wrap-theme']/div[2]/div[1]/wm-plans-page/div/div/div/wm-plans-authorization/div/div/wm-auth-page/div/ng-transclude/wm-auth/div/div[3]/wm-signup/div/div/form/input[4]");
	By createAccount = By.xpath(".//*[@id='wrap-theme']/div[2]/div[1]/wm-plans-page/div/div[1]/div/wm-plans-authorization/div/div/wm-auth-page/div/ng-transclude/wm-auth/div/div[3]/wm-signup/div/div/form/button");
	
	
	public WebElement username() {
		return driver.findElement(username);
	}


	public WebElement password() {
		return driver.findElement(password);
	}

	public static String randomPassword() {

		String letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789+@";

		String randomPassword = "";
		for (int i = 0; i < PASSWORD_LENGTH; i++) {
			int index = (int) (RANDOM.nextDouble() * letters.length());
			randomPassword += letters.substring(index, index + 1);
		}
		return randomPassword;
	}
	


	public WebElement createAccount() {
		return driver.findElement(createAccount);
	}
}
