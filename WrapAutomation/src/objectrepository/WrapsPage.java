package objectrepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WrapsPage {
	WebDriver driver;

	public WrapsPage(WebDriver driver) {
		this.driver = driver;
	}
	
By templateTab = By.xpath(".//*[@id='wrap-theme']/div[2]/div[1]/wm-main-layout/wm-wrap-nav-tabs/ul/li[1]/a");
By templateType = By.xpath(".//*[@id='wrap-theme']/div[2]/div[1]/wm-main-layout/div[2]/wm-template-layout-manager/wm-template-categories/div[1]/div[1]");
By createWrap = By.xpath(".//*[@id='wrap-theme']/div[2]/div[1]/wm-main-layout/div[2]/wm-template-layout-manager/div/wm-template-subcategories/wm-template-subcategory[1]/div/wm-card-group/ng-transclude/slot-actions/div/button[2]");
By closeOverlay = By.xpath(".//*[@id='wrap-theme']/div[5]/div/div/div/button[1]");
By publishWrap = By.xpath(".//*[@id='wrap-theme']/div[2]/div[1]/div/wm-wrap-editor/div/wm-action-bar/div/div[2]/div[9]/button");
	
public WebElement templateTab() {
		
		return driver.findElement(templateTab);

	}
	
	public WebElement templateType() {
		
		return driver.findElement(templateType);

	}
	
	public WebElement createWrap() {
		
		return driver.findElement(createWrap);

	}
	
	public WebElement closeOverlay() {
		
		return driver.findElement(closeOverlay);

	}
	
	public WebElement publishWrap() {
		
		return driver.findElement(publishWrap);

	}
}
