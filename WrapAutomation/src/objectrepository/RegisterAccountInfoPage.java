package objectrepository;

import java.util.UUID;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class RegisterAccountInfoPage {

	
	WebDriver driver;

	public RegisterAccountInfoPage(WebDriver driver) {
		this.driver = driver;
	}
	
	By firstName = By.xpath(".//*[@id='wrap-theme']/div[2]/div[1]/wm-plans-page/div/div/div/wm-plans-authorization/div/div/wm-auth-page/div/ng-transclude/wm-auth/div/div[3]/wm-signup/div/div/form/input[1]");
	By lastName = By.xpath(".//*[@id='wrap-theme']/div[2]/div[1]/wm-plans-page/div/div/div/wm-plans-authorization/div/div/wm-auth-page/div/ng-transclude/wm-auth/div/div[3]/wm-signup/div/div/form/input[2]");
	By company = By.xpath(".//*[@id='wrap-theme']/div[2]/div[1]/wm-plans-page/div/div/div/wm-plans-authorization/div/div/wm-auth-page/div/ng-transclude/wm-auth/div/div[3]/wm-signup/div/div/form/input[3]");
	
	By createAccount2 = By.xpath(".//*[@id='wrap-theme']/div[2]/div[1]/wm-plans-page/div/div/div/wm-plans-authorization/div/div/wm-auth-page/div/ng-transclude/wm-auth/div/div[3]/wm-signup/div/div/form/button");

	public WebElement firstName() {
		return driver.findElement(firstName);
	}
	
	 public String randomString(){
		 return  UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10);
	
		 }
	
	public WebElement lastName() {
		return driver.findElement(lastName);
	}

	public WebElement company() {
		return driver.findElement(company);
	}
	
	public WebElement createAccount2() {
		return driver.findElement(createAccount2);
	}
	
}
