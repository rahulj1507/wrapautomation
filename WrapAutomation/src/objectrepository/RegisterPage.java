package objectrepository;

import java.util.UUID;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RegisterPage {

	WebDriver driver;

	public RegisterPage(WebDriver driver) {
		this.driver = driver;
	}

	By email = By.xpath(
			".//*[@id='wrap-theme']/div[2]/div[1]/wm-plans-page/div/div/div/wm-plans-authorization/div/div/wm-auth-page/div/ng-transclude/wm-auth/div/div[3]/wm-signup/div/div/form/input");

	By signupButton = By.xpath(".//*[@id='wrap-theme']/div[2]/div[1]/wm-plans-page/div/div/div/wm-plans-authorization/div/div/wm-auth-page/div/ng-transclude/wm-auth/div/div[3]/wm-signup/div/div/form/button");
	
	public WebElement email() {
		return driver.findElement(email);
	}

	public String randomEmail() {
		return "email-" + UUID.randomUUID().toString() + "@test.com";
	}
	
	public WebElement signupButton() {
		return driver.findElement(signupButton);
	}
	
}
