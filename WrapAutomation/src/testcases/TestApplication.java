package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import objectrepository.LoginPage;
import objectrepository.PlansPage;
import objectrepository.Register2Page;
import objectrepository.RegisterAccountInfoPage;
import objectrepository.RegisterPage;
import objectrepository.WrapsPage;

public class TestApplication {

	WebDriver driver;

	@BeforeTest
	public void openBrowser() {

		driver = new FirefoxDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://authoring.qa.wrapdev.net/#/login");
	}

	@AfterTest
	public void closeBrowser() {
		driver.manage().deleteAllCookies();
		driver.quit();

	}

	@BeforeMethod
	public void implicitWait() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test(priority = 1)
	public void loginSignup() {

		LoginPage loginpageobj = new LoginPage(driver);
		loginpageobj.loginSignup().click();
	}

	@Test(priority = 2)
	public void signup() {
		PlansPage planspageobj = new PlansPage(driver);
		planspageobj.signup().click();
	}

	@Test(priority = 3)
	public void email() {
		RegisterPage registerpageobj = new RegisterPage(driver);
		String randomEmail = registerpageobj.randomEmail();
		registerpageobj.email().sendKeys(randomEmail);
	}

	@Test(priority = 4)
	public void signupButton() {
		RegisterPage registerpageobj = new RegisterPage(driver);
		registerpageobj.signupButton().click();
	}

	@Test(priority = 5)
	public void username() {
		try {
			Register2Page register2pageobj = new Register2Page(driver);
			Thread.sleep(2000);
			register2pageobj.username().click();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	@Test(priority = 6)
	public void password() {
		Register2Page register2pageobj = new Register2Page(driver);
		String randomPassword = Register2Page.randomPassword();
		register2pageobj.password().sendKeys(randomPassword);
	}

	@Test(priority = 7)
	public void createAccount() {
		Register2Page register2pageobj = new Register2Page(driver);
		register2pageobj.createAccount().click();
	}

	@Test(priority = 8)
	public void firstName() {
		RegisterAccountInfoPage accountinfopageobj = new RegisterAccountInfoPage(driver);
		String randomFirstName = accountinfopageobj.randomString();
		accountinfopageobj.firstName().sendKeys(randomFirstName);
	}

	@Test(priority = 9)
	public void lastame() {
		RegisterAccountInfoPage accountinfopageobj = new RegisterAccountInfoPage(driver);
		String randomLastName = accountinfopageobj.randomString();
		accountinfopageobj.lastName().sendKeys(randomLastName);
	}

	@Test(priority = 10)
	public void company() {
		RegisterAccountInfoPage accountinfopageobj = new RegisterAccountInfoPage(driver);
		String randomCompanyName = accountinfopageobj.randomString();
		accountinfopageobj.company().sendKeys(randomCompanyName);
	}

	@Test(priority = 11)
	public void createAccount2() {
		try {
			RegisterAccountInfoPage accountinfopageobj = new RegisterAccountInfoPage(driver);
			Thread.sleep(2000);
			accountinfopageobj.createAccount2().click();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Test(priority = 12)
	public void templateTab() {
		try {
			WrapsPage wrapspageobj = new WrapsPage(driver);
			Thread.sleep(5000);
			wrapspageobj.templateTab().click();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Test(priority = 13)
	public void templateType() {
		try {
			WrapsPage wrapspageobj = new WrapsPage(driver);
			Thread.sleep(3000);
			wrapspageobj.templateType().click();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Test(priority = 14)
	public void createWrap() {
		try {
			WrapsPage wrapspageobj = new WrapsPage(driver);
			Thread.sleep(5000);
			wrapspageobj.createWrap().click();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Test(priority = 15)
	public void closeOverlay() {
		try {
			WrapsPage wrapspageobj = new WrapsPage(driver);
			Thread.sleep(3000);
			wrapspageobj.closeOverlay().click();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Test(priority = 16)
	public void publishWrap() {
		try {
			WrapsPage wrapspageobj = new WrapsPage(driver);
			Thread.sleep(2000);
			wrapspageobj.publishWrap().click();
			WebDriverWait wait = new WebDriverWait(driver, 70);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='wrap-theme']/div[5]/div/div/div/button[1]")));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
