# README #

### What is this repository for? ###
*Wrap Automation Test

### How do I get set up? ###

*The project uses Page Object Model and TestNG to automate Wrap Media website.

*Environment for development: Eclipse, Selenium Jars 2.53 for Java (http://selenium-release.storage.googleapis.com/2.53/selenium-java-2.53.0.zip), Firefox build 43 and TestNG.

*Please note that Selenium 2.53 is not compatible with Firefox 47. https://github.com/SeleniumHQ/selenium/issues/2204 .

*I tested my work with Firefox 43. (https://ftp.mozilla.org/pub/firefox/releases/43.0b1/win64/en-US/Firefox%20Setup%2043.0b1.exe)



### Video recording ###
*Video recording for automation: https://www.youtube.com/watch?v=UG5RLnACmb8